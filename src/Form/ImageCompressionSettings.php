<?php
namespace Drupal\simple_media_image_compression\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Configure settings for this site.
*/
class  ImageCompressionSettings extends ConfigFormBase {
/**
 * {@inheritdoc}
 */
  public function getFormId() {
    return 'image_compression_setting';
  }

/**
 * {@inheritdoc}
 */
  protected function getEditableConfigNames() {
    return [
      'image_compression.qualitysettings',
    ];
  }

/**
 * {@inheritdoc}
 */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('image_compression.qualitysettings');

    $form['enable_compression'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Compression'),
      '#default_value' => $config->get('enable_compression'), // Set the default value to TRUE if you want compression enabled by default.
      //'#default_value' => TRUE, // Set the default value to TRUE if you want compression enabled by default.
    ];

    $form['jpegquality']=[
      '#type'=>'textfield',
      '#title'=>$this->t('Enter JPEG compression quality between <b>10-100</b>. Default value is <b>70</b>'),
      '#description'=>$this->t('Here 10 is lowest quality and 100 is highest  image quality'),
      '#default_value'=> empty($config->get('jpegquality'))? 70 : $config->get('jpegquality'),
    ];
    return parent::buildForm($form, $form_state);
  }

/**
 * {@inheritdoc}
 */
public function validateForm(array &$form, FormStateInterface $form_state) {
  $jpgvalue = trim($form_state->getValue('jpegquality'));

  if(empty( trim($jpgvalue))){
     $form_state->setValue('jpegquality',70);
  } else {
    if(!is_numeric( $jpgvalue)){
      $form_state->setErrorByName('jpegquality', $this->t('The JPG value must be a number.'));
    }elseif ( (( $jpgvalue < 10 )||  ( $jpgvalue >100))){
      $form_state->setErrorByName('jpegquality', $this->t('The JPG value must be between 10-100.'));
    }
  }

}


/**
 * {@inheritdoc}
 */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration
       $this->configFactory->getEditable('image_compression.qualitysettings')
      // Set the submitted configuration setting
      ->set('jpegquality', $form_state->getValue('jpegquality'))
      ->save();
      $this->configFactory->getEditable('image_compression.qualitysettings')
      // Set the submitted configuration setting
      ->set('enable_compression', $form_state->getValue('enable_compression'))
      ->save();
      parent::submitForm($form, $form_state);
  }
}